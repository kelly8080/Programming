#install.packages("dplyr")
require(data.table)
require(dplyr)

dt <- fread("amis.csv")
df <- dt[sample(x=1:nrow(dt),size=1e3)]

filter(df, df$speed>30 & df$warning==1) #filter
#arrange
#select
#mutate
#transmute
#summarise
